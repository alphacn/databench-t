/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.pojo;


public class Clustercfg {
    private String cluster_id; //集群id
    private String cluster_type; //集群类型master slave
    private String cluster_url; //集群业务url
    private String cluster_stat; //集群状态1启动0停止
    private String cluster_finish; //集群业务运行情况0未运行,1运行中
	public String getCluster_id() {
		return cluster_id;
	}
	public void setCluster_id(String cluster_id) {
		this.cluster_id = cluster_id;
	}
	public String getCluster_type() {
		return cluster_type;
	}
	public void setCluster_type(String cluster_type) {
		this.cluster_type = cluster_type;
	}
	public String getCluster_url() {
		return cluster_url;
	}
	public void setCluster_url(String cluster_url) {
		this.cluster_url = cluster_url;
	}
	public String getCluster_stat() {
		return cluster_stat;
	}
	public void setCluster_stat(String cluster_stat) {
		this.cluster_stat = cluster_stat;
	}
	public String getCluster_finish() {
		return cluster_finish;
	}
	public void setCluster_finish(String cluster_finish) {
		this.cluster_finish = cluster_finish;
	}
    
    
    
}
