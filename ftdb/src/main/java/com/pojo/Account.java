/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.pojo;

import java.math.BigDecimal;
import java.sql.Time;
import java.util.Date;

public class Account {

    private String account_id; //账号
    private String account_sjnoid; //科目号
    private String account_stat; //状态
    private String account_custid; //客户编号
    private BigDecimal  account_bale; //余额
    private String account_pswd; //密码
    private String account_name; //户名
    private Float  account_amrz; //积数
    private String account_inmod; //计息方式
    private Float  account_itrz; //利率
    private String account_branchid; //开户网点
    private Date   account_mdate; //最后修改日期
    private Time   account_mtime; //最后修改时间

    private String account_fld1; 
    private String account_fld2;
    private BigDecimal  account_fld3; 
    private BigDecimal  account_fld4;
    
    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public String getAccount_sjnoid() {
        return account_sjnoid;
    }

    public void setAccount_sjnoid(String account_sjnoid) {
        this.account_sjnoid = account_sjnoid;
    }

    public String getAccount_stat() {
        return account_stat;
    }

    public void setAccount_stat(String account_stat) {
        this.account_stat = account_stat;
    }

    public String getAccount_custid() {
        return account_custid;
    }

    public void setAccount_custid(String account_custid) {
        this.account_custid = account_custid;
    }

    public BigDecimal getAccount_bale() {
        return account_bale;
    }

    public void setAccount_bale(BigDecimal account_bale) {
        this.account_bale = account_bale;
    }

    public String getAccount_pswd() {
        return account_pswd;
    }

    public void setAccount_pswd(String account_pswd) {
        this.account_pswd = account_pswd;
    }

    public String getAccount_name() {
        return account_name;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    public Float getAccount_amrz() {
        return account_amrz;
    }

    public void setAccount_amrz(Float account_amrz) {
        this.account_amrz = account_amrz;
    }

    public String getAccount_inmod() {
        return account_inmod;
    }

    public void setAccount_inmod(String account_inmod) {
        this.account_inmod = account_inmod;
    }

    public Float getAccount_itrz() {
        return account_itrz;
    }

    public void setAccount_itrz(Float account_itrz) {
        this.account_itrz = account_itrz;
    }

    public String getAccount_branchid() {
        return account_branchid;
    }

    public void setAccount_branchid(String account_branchid) {
        this.account_branchid = account_branchid;
    }

    public Date getAccount_mdate() {
        return account_mdate;
    }

    public void setAccount_mdate(Date account_mdate) {
        this.account_mdate = account_mdate;
    }

    public Time getAccount_mtime() {
        return account_mtime;
    }

    public void setAccount_mtime(Time account_mtime) {
        this.account_mtime = account_mtime;
    }

	public String getAccount_fld1() {
		return account_fld1;
	}

	public void setAccount_fld1(String account_fld1) {
		this.account_fld1 = account_fld1;
	}

	public String getAccount_fld2() {
		return account_fld2;
	}

	public void setAccount_fld2(String account_fld2) {
		this.account_fld2 = account_fld2;
	}

	public BigDecimal getAccount_fld3() {
		return account_fld3;
	}

	public void setAccount_fld3(BigDecimal account_fld3) {
		this.account_fld3 = account_fld3;
	}

	public BigDecimal getAccount_fld4() {
		return account_fld4;
	}

	public void setAccount_fld4(BigDecimal account_fld4) {
		this.account_fld4 = account_fld4;
	}
    
    
}
