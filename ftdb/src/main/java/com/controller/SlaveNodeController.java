/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.common.context.SpringContextUtils;
import com.common.fastjson.JsonResult;
import com.service.impl.SlaveNodeServiceImpl;

@Controller
public class SlaveNodeController {

    private final Logger logger = LoggerFactory.getLogger(SlaveNodeController.class); 

    
    @RequestMapping(value = {"/begin"}, method = RequestMethod.POST)
    @ResponseBody
    public JsonResult begin(@RequestBody Map<String, String> param){
    	String datacfg_id = param.get("datacfg_id");
    	String process_id = param.get("process_id");
    	String trancfg_testid = param.get("trancfg_testid");
    	String cluster_id = param.get("cluster_id");
    	logger.info(" SlaveNodeController begin datacfg_id {} trancfg_testid {} process_id {} ",datacfg_id,trancfg_testid,process_id);
    	SlaveNodeServiceImpl slaveNodeServiceImpl = SpringContextUtils.getBean(SlaveNodeServiceImpl.class);
    	slaveNodeServiceImpl.setDatacfg_id(datacfg_id);
    	slaveNodeServiceImpl.setTrancfg_testid(trancfg_testid);
    	slaveNodeServiceImpl.setProcess_id(process_id);
    	slaveNodeServiceImpl.setCluster_id(cluster_id);
    	Thread thread = new Thread(slaveNodeServiceImpl); 
    	thread.start();
    	logger.info(" SlaveNodeController end datacfg_id {} trancfg_testid {} process_id {} ",datacfg_id,trancfg_testid,process_id);
    	return JsonResult.success();
    }
}
