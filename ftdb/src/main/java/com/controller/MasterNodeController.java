/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.controller;

import java.util.UUID;

import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.common.context.SpringContextUtils;
import com.common.fastjson.JsonResult;
import com.service.impl.MasterNodeServiceImpl;

@Controller
public class MasterNodeController {

    private final Logger logger = LoggerFactory.getLogger(MasterNodeController.class); 

    
    @RequestMapping(value = {"/business"})
    @ResponseBody
    public JsonResult begin(@Param(value = "datacfg_id")String datacfg_id,@Param(value = "datacfg_id")String trancfg_testid
    		,@Param(value = "isolationLevel")String isolationLevel){
    	String process_id = UUID.randomUUID().toString().replace("-", "");
    	logger.info(" MasterNodeController begin datacfg_id {} trancfg_testid {} process_id {} ",datacfg_id,trancfg_testid,process_id);
    	MasterNodeServiceImpl masterNodeServiceImpl = SpringContextUtils.getBean(MasterNodeServiceImpl.class);
    	masterNodeServiceImpl.setDatacfg_id(datacfg_id);
    	masterNodeServiceImpl.setTrancfg_testid(trancfg_testid);
    	masterNodeServiceImpl.setProcess_id(process_id);
    	masterNodeServiceImpl.setIsolationLevel(isolationLevel);
    	Thread thread = new Thread(masterNodeServiceImpl); 
    	thread.start();
    	logger.info(" MasterNodeController end datacfg_id {} trancfg_testid {} process_id {} ",datacfg_id,trancfg_testid,process_id);
    	return JsonResult.success(process_id);
    }
}
