/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.config;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import com.mchange.v2.c3p0.ComboPooledDataSource;

@Configuration
@MapperScan(basePackages = {"com.localMapper"}, sqlSessionTemplateRef = "ssqlSessionTemplate")
public class LocalConfiguration {

	@Value("${local.datasource.driverClassName}")
    private String driver;
    @Value("${local.datasource.url}")
    private String url;
    @Value("${local.datasource.username}")
    private String username;
    @Value("${local.datasource.password}")
    private String password;
    @Value("${local.datasource.initSize}")
    private int initSize;
    @Value("${local.datasource.minSize}")
    private int minSize;
    @Value("${local.datasource.maxSize}")
    private int maxSize;
    
    @Value("${local.mapper.path}")
    private String localMapperPath;
    
    @Bean(name = "sdataSource")
    @ConfigurationProperties(prefix = "local.datasource")
    public DataSource sdataSource() throws Exception {
		ComboPooledDataSource dataSource = new ComboPooledDataSource();
	    dataSource.setDriverClass(driver);
	    dataSource.setJdbcUrl(url);
	    dataSource.setUser(username);
	    dataSource.setPassword(password);
        dataSource.setAutoCommitOnClose(false);
	    dataSource.setInitialPoolSize(initSize);
	    dataSource.setMinPoolSize(minSize);
	    dataSource.setMaxPoolSize(maxSize);
        return dataSource;
    }
    @Bean(name = "localTransactionManager")
    public DataSourceTransactionManager masterTransactionManager() throws Exception {
        return new DataSourceTransactionManager(sdataSource());
    }
    @Bean
    public SqlSessionFactory ssqlSessionFactory(@Qualifier("sdataSource") DataSource dataSource){
        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();

        bean.setDataSource(dataSource);
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        try {
            bean.setMapperLocations(resolver.getResources(localMapperPath));
            return bean.getObject();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }

    @Bean
    public SqlSessionTemplate ssqlSessionTemplate(@Qualifier("ssqlSessionFactory") SqlSessionFactory sqlSessionFactory){
        SqlSessionTemplate template = new SqlSessionTemplate(sqlSessionFactory);
        return template;
    }
}
