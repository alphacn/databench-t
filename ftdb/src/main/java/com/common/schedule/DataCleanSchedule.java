/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.common.schedule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.common.constant.ParamConstant;
import com.localMapper.LocalTranLogMapper;
import com.localMapper.ParamcfgMapper;
import com.mapper.TranLogMapper;
import com.mapper.UniformityMapper;
import com.pojo.Paramcfg;
import com.pojo.Tranlog;

@Component
@Configuration      //1.主要用于标记配置类，兼备Component的效果。
public class DataCleanSchedule {
	
    private final Logger logger = LoggerFactory.getLogger(DataCleanSchedule.class); 

    @Autowired
    private UniformityMapper uniformityMapper;
    @Autowired
    private LocalTranLogMapper localTranLogMapper;
    @Autowired
    private TranLogMapper tranLogMapper;
    @Autowired
    private ParamcfgMapper paramcfgMapper;
    
    @Scheduled(cron = "0 0 1 * * ?")
    private void moveTranlog() {
    	if(!HeartbeatSchedule.isMaster) {
			return;
		}
        Paramcfg paramcfg = paramcfgMapper.getParamByName(ParamConstant.PARAM_UPDATE_MOVE_TRANLOG);
    	int num_count = Integer.parseInt(paramcfg.getParam_value());
    	long tranlog_strtime = System.currentTimeMillis();
    	Tranlog param = new Tranlog();
    	param.setTranlog_strtime(tranlog_strtime);
    	Long tranLogMapperNum = uniformityMapper.tranlogCount(param).longValue();
    	Long localTranLogMapperNum = localTranLogMapper.countTranlog(param);
    	if(tranLogMapperNum==null||tranLogMapperNum==0||localTranLogMapperNum==null||localTranLogMapperNum==0) {
    		return;
    	}
    	int frequency = (int) Math.ceil(tranLogMapperNum/(double)num_count);
    	int frequencyLocal = (int) Math.ceil(localTranLogMapperNum/(double)num_count);
    	try {
    		for (int i = 0; i < frequency; i++) {
    			logger.info(" ScheduleTask moveTranlog tranLogMapper insertTranlogHistory begin {} ",i);
            	tranLogMapper.insertTranlogHistory(num_count, tranlog_strtime);
        		logger.info(" ScheduleTask moveTranlog tranLogMapper insertTranlogHistory end  {} ",i);
        		logger.info(" ScheduleTask moveTranlog tranLogMapper deleteTranlog begin {} ",i);
            	tranLogMapper.deleteTranlog(num_count, tranlog_strtime);
        		logger.info(" ScheduleTask moveTranlog tranLogMapper deleteTranlog end {} ",i);
			}
		} catch (Exception e) {
			logger.error(" ScheduleTask moveTranlog tranLogMapper error {} ",e);
		}
    	try {
    		for (int i = 0; i < frequencyLocal; i++) {
    			logger.info(" ScheduleTask moveTranlog localTranLogMapper insertTranlogHistory begin {} ",i);
        	  	localTranLogMapper.insertTranlogHistory(num_count, tranlog_strtime);
        	  	logger.info(" ScheduleTask moveTranlog localTranLogMapper insertTranlogHistory end {} ",i);
        		logger.info(" ScheduleTask moveTranlog localTranLogMapper deleteTranlog begin {} ",i);
            	localTranLogMapper.deleteTranlog(num_count, tranlog_strtime);
        		logger.info(" ScheduleTask moveTranlog localTranLogMapper deleteTranlog end  {} ",i);
    		}
		} catch (Exception e) {
			logger.error(" ScheduleTask moveTranlog tranLogMapper error {} ",e);
		}

    }
}
